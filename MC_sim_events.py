#import ROOT as root
import numpy as np
import sys
import os
import math
import pandas as pd
import tqdm

lb = 0.001
collision = int(lb*60*40*1e6)
roll_over = 3564

#read filled ATLAS bunches (now file contains one line of BX numbers, separated by commas)
filename_filled_bx = sys.argv[1]
#df_in = pd.read_csv("filled_bunches.csv", sep=', ',header =None, engine='python')
df_in = pd.read_csv(filename_filled_bx, sep=', ',header =None, engine='python')
filled_bunches = df_in.values.tolist()

#prepare for data
data_bcid = []
data_bcid_existance = []
data_trains = []

for i in tqdm.tqdm(range (collision)):
    #ok bx numbers
    if (i % roll_over == 0):
        bx = 0
    else:
        bx = i % roll_over
    #check for filled bunches
    try:
        filled_bunches[0].index(bx)
        data_bcid.append(bx)
        rnd_bx = np.random.rand(1)
        if rnd_bx < 0.8: #probability to have event in BX
            data_bcid_existance.append(1)
            rnd_train = np.random.randint(4) #one of trains
            if rnd_train == 0:
                data_trains.append("1000")
            if rnd_train == 1:
                data_trains.append("0100")
            if rnd_train == 2:
                data_trains.append("0010")
            if rnd_train == 3:
                data_trains.append("0001")
        else:
            data_bcid_existance.append(0)
            data_trains.append("0000")
    except ValueError:
        a = "NONE"

#output    
df_out = pd.DataFrame({'BCID num': data_bcid,
                   'BCID event': data_bcid_existance,
                   'Trains': data_trains})

df_out.to_csv("sim_0_001LB.csv", index=False)





