import ROOT as root
import numpy as np
import sys
import os
import math
import tqdm
import pandas as pd

#equations to get positions in NEAR and FAR stations
def transport(f_px,f_py,f_pz,f_parameters):
    f_dist = float(12.085)
    f_coordinates_near = [] #x, y, x_bar(angle), y_bar(angle)
    f_coordinates_far = []
    xi = 1 - float(abs(f_pz)/6800)
    sx = f_px/abs(f_pz)
    sy = f_py/abs(f_pz)
    for var in range(4):
        A = float(0)
        E = float(0)
        F = float(0)
        for mult_A in range(len(f_parameters[var][0])):
            A = A + f_parameters[var][0][mult_A]*pow(xi,mult_A)
        for mult_E in range(len(f_parameters[var][4])):
            E = E + f_parameters[var][4][mult_E]*pow(xi,mult_E)
        for mult_F in range(len(f_parameters[var][5])):
            F = F + f_parameters[var][5][mult_F]*pow(xi,mult_F)
        coord = A + E*sx + F*sy
        f_coordinates_near.append(coord)
    f_coordinates_far.append((f_coordinates_near[0] + f_coordinates_near[2]*f_dist))
    f_coordinates_far.append((f_coordinates_near[1] + f_coordinates_near[3]*f_dist))
    f_coordinates_far.append(f_coordinates_near[2])
    f_coordinates_far.append(f_coordinates_near[3])
    return f_coordinates_far

#read momentum from pythia
print("Reading pythia output")
px = []
py = []
pz = []

filename_pythia = sys.argv[1]
file_pythia = open(filename_pythia, 'r')
Lines = file_pythia.readlines()
for line in Lines:
    if "Protons" in line:
        full = line.split()
        px.append(float(full[1]))
        py.append(float(full[2]))
        pz.append(float(full[3]))
    if "Eff" in line:
        line_eff = line.split()
        eff = float(line_eff[1])/100

file_pythia.close()

#read parametrisation; actually in the end only 0, 4, 5 will exist
#x, y, x_bar(angle), y_bar(angle)
#A, B, C, D, E, F, G, H
print("Reading parameters")
#Beam1 (side C): pz>0
filename_parameters_b1 = sys.argv[2]
parameters_b1 = [[[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[]]]
gen_count = -1
local_count = 0
file_parametrization_b1 = open(filename_parameters_b1, 'r')
Lines = file_parametrization_b1.readlines()
for line in Lines:
    if "@" in line:
        gen_count = gen_count + 1
        local_count = 0
        continue
    if gen_count >= 0:
        full = line.split()
        mult = int(full[0])
        for i in range (1,mult+2):
            parameters_b1[gen_count][local_count].append(float(full[i]))
        local_count = local_count +1
file_parametrization_b1.close()
#Beam2 (side A): pz<0
filename_parameters_b2 = sys.argv[3]
parameters_b2 = [[[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[]]]
gen_count = -1
local_count = 0
file_parametrization_b2 = open(filename_parameters_b2, 'r')
Lines = file_parametrization_b2.readlines()
for line in Lines:
    if "@" in line:
        gen_count = gen_count + 1
        local_count = 0
        continue
    if gen_count >= 0:
        full = line.split()
        mult = int(full[0])
        for i in range (1,mult+2):
            parameters_b2[gen_count][local_count].append(float(full[i]))
        local_count = local_count +1
file_parametrization_b2.close()

#gennerate events according to pile-up and get positions
print("Generating events")
proton = 0
pileUp = 35
coordinates_far_B1_C = [[],[],[],[]] #x, y, x_bar(angle), y_bar(angle)
coordinates_far_B2_A = [[],[],[],[]]
random = root.TRandom3()
for i in tqdm.tqdm(range(10000)):
    vrtx = random.Poisson(pileUp)
    for j in range(vrtx):
        rnd_bx = np.random.rand(1)
        if rnd_bx < eff:
            if pz[proton]>0:
                tmp_coor = transport(px[proton],py[proton],pz[proton],parameters_b1)
                proton = proton + 1
                for var in range(4):
                    coordinates_far_B1_C[var].append(tmp_coor[var])
            elif pz[proton]<0:
                tmp_coor = transport(px[proton],py[proton],pz[proton],parameters_b2)
                proton = proton + 1
                for var in range(4):
                    coordinates_far_B2_A[var].append(tmp_coor[var])

#get probabilies
print("Get probabilities")
beam_det_dist_B2_A =2.47 + 0.3 + 0.2
beam_det_dist_B1_C = 2.08 + 0.3 + 0.2
width_tr = [2, 4, 5, 5.5]
addings_tr = [2, 6, 11, 16.5]
events_tr_A = [0,0,0,0]
events_tr_C = [0,0,0,0]

#C
for i in range(len(coordinates_far_B1_C[0])):
    if (abs(coordinates_far_B1_C[0][i]*1000) >= beam_det_dist_B1_C and abs(coordinates_far_B1_C[0][i]*1000) < beam_det_dist_B1_C+addings_tr[0]):
        events_tr_C[0] = events_tr_C[0] + 1
    elif (abs(coordinates_far_B1_C[0][i]*1000) >= beam_det_dist_B1_C+addings_tr[0] and abs(coordinates_far_B1_C[0][i]*1000) < beam_det_dist_B1_C+addings_tr[1]):
        events_tr_C[1] = events_tr_C[1] + 1
    elif (abs(coordinates_far_B1_C[0][i]*1000) >= beam_det_dist_B1_C+addings_tr[1] and abs(coordinates_far_B1_C[0][i]*1000) < beam_det_dist_B1_C+addings_tr[2]):
        events_tr_C[2] = events_tr_C[2] + 1
    elif (abs(coordinates_far_B1_C[0][i]*1000) >= beam_det_dist_B1_C+addings_tr[2] and abs(coordinates_far_B1_C[0][i]*1000) < beam_det_dist_B1_C+addings_tr[3]):
        events_tr_C[3] = events_tr_C[3] + 1
#A
for i in range(len(coordinates_far_B2_A[0])):
    if (abs(coordinates_far_B2_A[0][i]*1000) >= beam_det_dist_B2_A and abs(coordinates_far_B2_A[0][i]*1000) < beam_det_dist_B2_A+addings_tr[0]):
        events_tr_A[0] = events_tr_A[0] + 1
    elif (abs(coordinates_far_B2_A[0][i]*1000) >= beam_det_dist_B2_A+addings_tr[0] and abs(coordinates_far_B2_A[0][i]*1000) < beam_det_dist_B2_A+addings_tr[1]):
        events_tr_A[1] = events_tr_A[1] + 1
    elif (abs(coordinates_far_B2_A[0][i]*1000) >= beam_det_dist_B2_A+addings_tr[1] and abs(coordinates_far_B2_A[0][i]*1000) < beam_det_dist_B2_A+addings_tr[2]):
        events_tr_A[2] = events_tr_A[2] + 1
    elif (abs(coordinates_far_B2_A[0][i]*1000) >= beam_det_dist_B2_A+addings_tr[2] and abs(coordinates_far_B2_A[0][i]*1000) < beam_det_dist_B2_A+addings_tr[3]):
        events_tr_A[3] = events_tr_A[3] + 1

prob_A = []
prob_C = []
prob = []
for i in range(4):
    prob_A.append(events_tr_A[i]/len(coordinates_far_B2_A[0]))
    prob_C.append(events_tr_C[i]/len(coordinates_far_B1_C[0]))
    prob.append( (events_tr_A[i]+events_tr_C[i]) / (len(coordinates_far_B2_A[0])+len(coordinates_far_B1_C[0])))

print("C")           
print(prob_C)
print(prob_C[0]+prob_C[1]+prob_C[2]+prob_C[3])
print(abs(min(coordinates_far_B1_C[0])*1000))
print(abs(max(coordinates_far_B1_C[0])*1000))
print(beam_det_dist_B1_C)
print(beam_det_dist_B1_C+addings_tr[3])

print("A")           
print(prob_A)
print(prob_A[0]+prob_A[1]+prob_A[2]+prob_A[3])
print(abs(min(coordinates_far_B2_A[0])*1000))
print(abs(max(coordinates_far_B2_A[0])*1000))
print(beam_det_dist_B2_A)
print(beam_det_dist_B2_A+addings_tr[3])