#include "Pythia8/Pythia.h"
using namespace Pythia8;
int main() {
    // Generator. Process selection. LHC initialization.
    Pythia pythia;
    Event& event = pythia.event;
    pythia.readString("Beams:eCM = 13600.");
    pythia.readString("SoftQCD:nonDiffractive = on");
    pythia.readString("SoftQCD:singleDiffractive = on");
    pythia.readString("SoftQCD:doubleDiffractive = on");
    pythia.readString("SoftQCD:centralDiffractive = on");
    pythia.init();
    int n_protons = 0;
    int n_gen = 0;
    for (int iEvent = 0; iEvent < 10000; ++iEvent) 
    {
        if (!pythia.next()) continue;
        ++n_gen;
        for (int i = 0; i < event.size(); ++i)
        {
            if (event[i].id() == 2212 && event[i].isFinal() && event[i].e() > 5984 && event[i].e() < 6732)
            //if (event[i].id() == 2212 && event[i].isFinal())
            {
                ++n_protons;
                cout<<"Protons "<<event[i].px()<<" "<<event[i].py()<<" "<<event[i].pz()<<endl;
            } 
        }
    }
    float eff = static_cast<float>(n_protons)/static_cast<float>(n_gen)*100;
    cout<<"Eff "<<eff<<" General "<<n_gen<<" Ok "<<n_protons<<endl;
    pythia.stat();
    return 0;
}