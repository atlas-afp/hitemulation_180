import numpy as np
import sys
import os
import math
import pandas as pd
import tqdm

roll_over = 3564

#read input simulated file
filename_sim = sys.argv[1]
dtype = {"BCID num": int, "BCID event": bool, "Trains": str}
#df_in = pd.read_csv("sim_0_001LB.csv", dtype=dtype)
df_in = pd.read_csv(filename_sim, dtype=dtype)
in_bcid = df_in['BCID num'].tolist()
in_bcid_existance = df_in['BCID event'].tolist()
in_trains = df_in['Trains'].tolist()

#prepare for output
orb = []
creq = []
ttyp = []
bgo = []
ttr = []
mult = []

prev_bcid = 5000
prev_str = "000000x0000000000"
for i in tqdm.tqdm(range (len(in_bcid))):
    #if filled bunches
    if ((in_bcid[i] > prev_bcid and in_bcid[i] != prev_bcid+1) or (in_bcid[i] < prev_bcid and prev_bcid < roll_over)):
        orb.append(0)
        creq.append("0000")
        ttyp.append("0x00")
        bgo.append("0000")
        ttr.append("0000")
        prev_str = "000000x0000000000"
        if (in_bcid[i] > prev_bcid):
            mult.append(in_bcid[i] - prev_bcid - 1)
        if (in_bcid[i] < prev_bcid):
            mult.append(roll_over - prev_bcid)

    #if next orbit
    if (in_bcid[i] < prev_bcid):
        orb.append(1)
    else:
        orb.append(0)
    creq.append("0000")
    ttyp.append("0x00")
    #if event is empty
    if not (in_bcid_existance[i]):
        bgo.append("0000")
        ttr.append("0000")
    else:
        trains_current_list = list(in_trains[i])
        bgo.append("{}000".format(trains_current_list[3]))
        ttr.append("{}{}{}0".format(trains_current_list[2],trains_current_list[1],trains_current_list[0]))
    mult.append(1)
    #if same type of event
    current_str = str(orb[-1]) + creq[-1] + ttyp[-1] + bgo[-1] + ttr[-1]
    if ((current_str == prev_str) ):
        orb.pop()
        creq.pop()
        ttyp.pop()
        bgo.pop()
        ttr.pop()
        mult.pop()
        mult[-1]=mult[-1]+1
    prev_bcid = in_bcid[i]
    prev_str = current_str


#output  
header = ["#-----------------------", "#                      M", "#                      u", "#                      l",
        "#                      t", "#                      i", "#                      p", "#                      l",
        "#   CCC                i", "#  BRRR    T BBBB TTT  c", "#O UEEE    T GGGG TTTL i", "#R SQQQ    Y OOOO RRR1 t", 
        "#B Y210    P 3210 321A y", "#------------------------"]

with open("sim_step2_0_001LB.dat", "w") as file_out:
    for line in header:
        file_out.write(line)
        file_out.write("\n")

df_out = pd.DataFrame({'ORB': orb,
                   'CREQ': creq,
                   'TTYP': ttyp,
                   'BGo': bgo,
                   'TTR': ttr,
                   'mult': mult})

df_out.to_csv("sim_step2_0_001LB.dat", sep=' ', header=False, index=False, mode='a')
