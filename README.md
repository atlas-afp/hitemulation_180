1. Prepare files: 
- 1.1  with filled buches for a given run. Example in filled_bunches.csv
- 1.2  with parametrization for later Transport calculation (can be taken from Twiki https://twiki.cern.ch/twiki/bin/viewauth/Atlas/AFPOptics#Run_3_Optics). Examples in parametrisation_<beamN>_<betta>_<tetta>.txt
2. Simulate  protons with Pythia (was used version 8.308). 
Source file: Pythia_protons.cc
This will create output file. Alredy created example: Pythia_output.txt
3. Use parametrized transport to get positions at the AFP FAR station location and calculate probabilities of having a proton in a given train: Transport.py. 
Run with python3 and input files: 
`python3 Transport.py <Pythia_output> <parametrisation_B1> <parametrisation_B2>`
Printed output: probabilities of having a proton in a given train.
4. Simulate events with python and given probabilities: MC_sim_events.py, use input file with filled bunches, probabilities hardcoded. 
Output: sim_0_001LB.csv
5. Format events in machine-readable file: MC_file_pg_alti.py, use input file from step 4. 
Output: sim_step2_0_001LB.dat
